export interface Animal {
    name: string;
    weight: string;
    speed: string;
}
