import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'Une belle liste d\'animaux';
    typeArray = [
        'Element1',
        456,
        'hello'
    ];
    logConsole(data) {
        console.log(data);
    }
}



